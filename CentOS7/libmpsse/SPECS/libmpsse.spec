# The (empty) main package is arch, to have the package built and tests run
# on all arches, but the actual result package is the noarch -devel subpackge.
# Debuginfo packages are disabled to prevent rpmbuild from generating an empty
# debuginfo package for the empty main package.
%global debug_package %{nil}

%global commit 5a0156e40feb

Name:           libmpsse
Version:        1.3.2
Release:        1%{?dist}
Summary:        Library for interfacing with SPI/I2C devices via FTDI\'s FT-2232 family of USB to serial chips.

Group:          Development/Libraries
License:        BSD
URL:            https://github.com/l29ah/libmpsse
Source0:        https://github.com/l29ah/libmpsse/archive/refs/tags/v%{version}.tar.gz

Requires: libftdi

BuildRequires: autoconf
BuildRequires: automake
BuildRequires: make
BuildRequires: gcc
BuildRequires: swig
BuildRequires: which
BuildRequires: libftdi-devel
BuildRequires: python-devel

%description
%{summary}

%package devel
Summary:   Library for interfacing with SPI/I2C devices via FTDI\'s FT-2232 family of USB to serial chips.
Group:     Development/Libraries
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description devel
%{summary}.

%package static
Summary:   Static library for libmpsse.
Group:     Development/Libraries
Requires: %{name}-devel = %{version}-%{release}

%description static
%{summary}.

%package python
Summary:   Python bindings for libmpsse.
Group:     Development/Libraries
Requires: %{name} = %{version}-%{release}

%description python
%{summary}.

%prep
%setup -q

%build
cd src
autoreconf
%configure
%{__make} %{?_smp_mflags} CFLAGS="-std=c99 -fPIC" PYLIB=${python_sitearch}

%install
cd src
%make_install PYLIB=%{python_sitearch}

%files
%{_libdir}/libmpsse.so

%files static
%{_libdir}/libmpsse.a

%files devel
%license docs/LICENSE
%{_includedir}/mpsse.h

%files python
%{python_sitearch}/*

%changelog
* Mon May 24 2021 Karol Krizka <kkrizka@gmail.com> - 1.3.2-1
- Create RPM
